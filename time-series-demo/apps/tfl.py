import requests
import sys
import os
import tempfile
import numpy as np
import os.path as path
import pandas as pd
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, WhiteKernel, ExpSineSquared
import matplotlib.pyplot as plt


def create_output_dir(p):
    try:
        os.makedirs(p)
    except FileExistsError:
        pass

def __pull_data_from_alchera(api_key):
    ALCHERA_QUERY_API_URL = ' https://stream.dev.alchera.tech/query/'

    get_provider_request_result = requests.get(url=ALCHERA_QUERY_API_URL + 'providers/',
                                               headers={'X-API-KEY': api_key})

    if get_provider_request_result.status_code != 200:
        get_provider_request_result.raise_for_status()

    # Get the sensors for a pre configured area in central London operated by TFL (provider id 1)
    filter_sensors_request_result = requests.post(url=ALCHERA_QUERY_API_URL + 'sensors/',
                                                  headers={'X-API-KEY': api_key},
                                                  json={
                                                      "provider_id": 1,
                                                      "fence": {
                                                          "type": "polygon",
                                                          "vertices": [
                                                              {
                                                                  "latitude": 51.571173,
                                                                  "longitude": -0.142625
                                                              },
                                                              {
                                                                  "latitude": 51.570882,
                                                                  "longitude": -0.059566
                                                              },
                                                              {
                                                                  "latitude": 51.537739,
                                                                  "longitude": -0.067438
                                                              },
                                                              {
                                                                  "latitude": 51.536244,
                                                                  "longitude": -0.139364
                                                              }
                                                          ]
                                                      }
                                                  })


    if filter_sensors_request_result.status_code != 200:
        filter_sensors_request_result.raise_for_status()

    number_of_sensors_found = len(filter_sensors_request_result.json().get('sensors'))
    sensor_ids = [sensor.get('id') for sensor in filter_sensors_request_result.json().get('sensors')]

    files = []
    for i in range(number_of_sensors_found):
        measurements_request_response = requests.post(url=ALCHERA_QUERY_API_URL + 'sensors/data/',
                                                      headers={'X-API-KEY': api_key},
                                                      json={
                                                          'sensors': [sensor_ids[i]],
                                                          'start_time': '2019-04-08T08:00:00Z',
                                                          'end_time': '2019-04-09T17:00:00Z',
                                                          'limit': 10000,
                                                          'response_type' : 'csv'
                                                      })

        if measurements_request_response.status_code != 200:
            measurements_request_response.raise_for_status()

        fd, filename = tempfile.mkstemp()
        with open(filename, 'wb') as file:
            for chunk in measurements_request_response:
                file.write(chunk)

        files.append(filename)

    combined_csv = pd.concat([pd.read_csv(f) for f in files])
    out_path = path.join(path.dirname(path.abspath(__file__)), '../datasets/tfl.csv')
    combined_csv.to_csv(out_path, index=False, encoding='utf-8-sig')


def get_example_data(api_key=None):
    tfl_path = path.join(path.dirname(path.abspath(__file__)), '../datasets/tfl.csv')
    if not path.isfile(tfl_path):
        print("Data file not found. Pulling from Alchera...")

        if api_key is None:
            raise Exception("You need to provide the API key for pulling data from Alchera.")

        __pull_data_from_alchera(api_key)
        print("Data collected...")

    tfl_data = pd.read_csv(tfl_path)
    # We should not have NaNs but just in case
    tfl_data = tfl_data.dropna(axis='rows')
    tfl_data['timestamp'] = pd.to_datetime(tfl_data['timestamp'], format='%Y-%m-%d %H:%M:%S')
    return tfl_data


def convert_lat_lo_xy(lat, lo):
    """
    Project latitude longitude to cartesian
    """
    x = lo * 6371000.0 * np.pi / 180.0
    y = np.log(np.tan(np.pi / 4.0 + (lat * np.pi / 360.0))) * 6371000.0

    return x, y


class DateTimeScalar(object):
    def __init__(self, low, high):
        # Convert to seconds
        self.low = low.value / 1000000.0
        self.high = high.value / 1000000.0

    def scale(self, t):
        input = t.value / 1000000.0
        scaled = (input - self.low) / (self.high - self.low)
        return scaled


if __name__ == "__main__":
    api_key = None
    if len(sys.argv) > 1:
        api_key = sys.argv[1]
    tfl_data = get_example_data(api_key=api_key)
    cut_off = pd.to_datetime('2019-04-09 15:00:00')  # Train till 3 PM data

    tfl_data = tfl_data[['sensor', 'timestamp', 'latitude', 'longitude', 'car_count_normalised']]

    time_scaler = DateTimeScalar(tfl_data['timestamp'].min(), tfl_data['timestamp'].max())
    tfl_data['time'] = tfl_data.apply(lambda x: time_scaler.scale(x.timestamp), axis=1)
    tfl_data[['x', 'y']] = tfl_data.apply(lambda x:
                                          pd.Series(convert_lat_lo_xy(x.latitude, x.longitude)), axis=1)

    all_x = np.array([tfl_data['x'], tfl_data['y'], tfl_data['time']]).T
    all_y = np.array(tfl_data['car_count_normalised'])
    train = tfl_data[tfl_data['timestamp'] <= cut_off]

    x = np.array([train['x'], train['y'], train['time']]).T
    y = np.array(train['car_count_normalised'])

    # Now we can load the stuff into the GP model and fit
    kernel = C(0., (1e-3, 1e3)) * RBF(17000, (1e-3, 1e6)) * ExpSineSquared(length_scale=150,
                                                                        periodicity=1440) + WhiteKernel(11e-4)
    gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=2)
    print("Fitting the model...")
    with np.errstate(divide='ignore'):
        gp.fit(x, y.reshape(-1,1))  # Fit using training data
    print("Done fitting...")

    out_path = path.join(path.dirname(path.abspath(__file__)), '../outputs')
    create_output_dir(out_path)

    sensors = tfl_data['sensor'].unique()
    for sensor_id in sensors:
        selected_sensor = tfl_data[tfl_data['sensor'] == sensor_id]
        test_x = np.array([selected_sensor['x'], selected_sensor['y'], selected_sensor['time']]).T
        test_y = np.array(selected_sensor['car_count_normalised'])
        # Do the prediction on everything
        y_pred, sigma = gp.predict(test_x, return_std=True)
        fig = plt.figure(figsize=(20, 10))
        plt.plot(selected_sensor['timestamp'], selected_sensor['car_count_normalised'],
                 'r.', markersize=5, label=u'Observations')
        plt.plot(selected_sensor['timestamp'], y_pred, 'b-', label=u'Prediction')

        plt.fill_between(selected_sensor['timestamp'],
                         np.squeeze(y_pred) - sigma,
                         np.squeeze(y_pred) + sigma,
                         color='darkorange',
                         alpha=0.2)
        fig.suptitle('Sensor: ' + str(sensor_id))
        plt.legend(loc='upper left')
        plt.savefig(out_path + '/' + str(sensor_id) + '_prediction.png')

    print("All figures are saved in: ", out_path)
























