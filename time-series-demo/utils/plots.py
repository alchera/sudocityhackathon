import os.path as path
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.animation as animation
from matplotlib.patches import Polygon

def generate_us_animation(flt, states, output_path):
    """
    Just add the plotting function for the US pollutant animation here
    for minimizing clutter
    :param flt: pandas Dataframe
    """
    shape_path = path.join(path.dirname(path.abspath(__file__)), '../shape/st99_d00')
    fig = plt.figure(figsize=(12, 10))
    ax1 = fig.add_subplot(221)
    ax1.title.set_text('NO2 AQI')
    map = Basemap(llcrnrlon=-119, llcrnrlat=22, urcrnrlon=-64, urcrnrlat=49, projection='lcc', lat_1=33, lat_2=45,
                  lon_0=-95)
    map.readshapefile(shape_path, name='states', drawbounds=True)
    ax2 = fig.add_subplot(222)
    ax2.title.set_text('O3 AQI')
    map = Basemap(llcrnrlon=-119, llcrnrlat=22, urcrnrlon=-64, urcrnrlat=49, projection='lcc', lat_1=33, lat_2=45,
                  lon_0=-95)
    map.readshapefile(shape_path, name='states', drawbounds=True)
    ax3 = fig.add_subplot(223)
    ax3.title.set_text('SO2 AQI')
    map = Basemap(llcrnrlon=-119, llcrnrlat=22, urcrnrlon=-64, urcrnrlat=49, projection='lcc', lat_1=33, lat_2=45,
                  lon_0=-95)
    map.readshapefile(shape_path, name='states', drawbounds=True)
    ax4 = fig.add_subplot(224)
    ax4.title.set_text('CO AQI')
    map = Basemap(llcrnrlon=-119, llcrnrlat=22, urcrnrlon=-64, urcrnrlat=49, projection='lcc', lat_1=33, lat_2=45,
                  lon_0=-95)
    map.readshapefile(shape_path, name='states', drawbounds=True)
    state_names = list()
    for shape_dict in map.states_info:
        state_names.append(shape_dict['NAME'])

    fig.suptitle('AQIs in US during ' + flt.loc[states[0]].index[0].strftime('%Y-%m'))

    cmap, norm = mpl.colors.from_levels_and_colors([np.nan, 0, 50, 100, 150, 200, 300, 500],
                                                   ['white', 'green', 'yellow', 'orange', 'red', 'purple', 'maroon'])
    m = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
    pollutants = ['NO2', 'O3', 'SO2', 'CO']
    axs = [ax1, ax2, ax3, ax4]

    num_records = flt.loc[states[0]].shape[0]

    def update(i):
        """
        Function called by the animator at each time step
        """
        for p, a in zip(pollutants, axs):
            for patch in reversed(a.patches):
                patch.remove()

            for state in states:
                if state != 'District Of Columbia':
                    color = m.to_rgba(flt.loc[state].iloc[i][p + ' AQI'])
                    seg = map.states[state_names.index(state)]
                    poly = Polygon(seg, facecolor=color, edgecolor=color)
                    a.add_patch(poly)

        fig.suptitle('AQIs in US during ' + flt.loc[states[0]].index[i].strftime('%Y-%m'))

    plt.tight_layout()
    anim = animation.FuncAnimation(fig, update, interval=100, blit=False, repeat=True, frames=num_records)
    try:
        anim.save(output_path, writer='imagemagick')
    except:
        print("Creating animation failed. You probably need to install imagemnagick.")