import websocket


def start_websocket(websocket_url, api_key):
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp(url=websocket_url,
                                header={'x-api-key': api_key},
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close,
                                on_open=on_open)
    ws.run_forever(ping_interval=30, ping_timeout=10)


def on_message(ws, message):
    print('Message received via WebSocket: ' + str(message))
    # Add code here to do something with each message as it is received


def on_error(ws, error):
    print('WebSocket Error:' + str(error))


def on_close(ws):
    print("### WebSocket Closed ###")


def on_open(ws):
    print("### WebSocket Opened ###")
    ws.send(SUBSCRIBE_TO_ALL_SENSORS_MESSAGE)


ALCHERA_WEBSOCKET_URL = 'wss://stream.dev.alchera.tech/query/subscriptions/'
YOUR_API_KEY = 'YOUR_API_KEY_GOES_HERE'

SUBSCRIBE_TO_ALL_SENSORS_MESSAGE = '{"subscribe": {}}'
SUBSCRIBE_TO_SINGLE_SENSORS_MESSAGE = '{"subscribe": {"sensor_id": 401}}'
SUBSCRIBE_TO_MULTIPLE_SENSORS_MESSAGE = '{"subscribe": {"sensors": [401, 789]}}'


if __name__ == "__main__":

    start_websocket(websocket_url=ALCHERA_WEBSOCKET_URL, api_key=YOUR_API_KEY)
