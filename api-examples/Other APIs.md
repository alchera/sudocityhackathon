# Smart Cambridge API

Collects a variety of real-time and historical data around Cambridge, including air quality, parking, and journey times.

Start by signing up at https://smartcambridge.org/accounts/signup/

Documentation is at https://smartcambridge.org/api/

# TfL API

Start by signing up at https://api-portal.tfl.gov.uk/signup

After signing in head to API credentials to retrieve your API key.

Documentation is at https://api.tfl.gov.uk/

# Others
* 511on.ca API https://511on.ca/developers/doc

You can find a lot of open data on the internet just by googling.

# Useful tools
## Postman
https://getpostman.com/

Helps you easily try out APIs by allowing you to make fully configurable HTTP requests.

## ngrok
https://ngrok.com/

Useful to expose server applications running on your machine to the internet or to have publicly reachable endpoints to implement webhooks.