import os
import os.path as path
import pandas as pd
import statsmodels.api as sm
import matplotlib.pyplot as plt
from fbprophet import Prophet
from ..utils.plots import generate_us_animation


def create_output_dir(p):
    try:
        os.makedirs(p)
    except FileExistsError:
        pass

def load_pollution_dataset():
    """
    Load and filter the pollution dataset.
    :return: pandas.DataFrame
        The filtered pollution dataset grouped by state and time.
    """
    # Load the CSV file using pandas
    poll = pd.read_csv(path.join(path.dirname(path.abspath(__file__)), '../datasets/pollution.csv'))
    # Just select the necessary columns
    poll_filtered = poll[['State','Date Local','NO2 AQI','O3 AQI','SO2 AQI','CO AQI']]
    # Drop rows with NA. One could use other strategy for replacing NAs
    poll_filtered = poll_filtered.dropna(axis='rows')
    # Just focus on US for this example
    poll_filtered = poll_filtered[poll_filtered.State != 'Country Of Mexico']
    # Change date string to date value
    poll_filtered['Date Local'] = pd.to_datetime(poll_filtered['Date Local'], format='%Y-%m-%d')
    # Take mean as there are duplicate values for some reason in the dataset
    poll_filtered = poll_filtered.groupby(['State', 'Date Local']).mean()
    return poll_filtered


def plot_global_mean(pds):
    """
    Plot the global pollutant trend.
    """
    out_path = path.join(path.dirname(path.abspath(__file__)), '../outputs')
    create_output_dir(out_path)
    f, ax = plt.subplots(figsize=[10, 4])
    pds.groupby(pd.Grouper(level='Date Local', freq='Y')).agg({'SO2 AQI': 'mean',
                                                               'CO AQI': 'mean',
                                                               'NO2 AQI': 'mean',
                                                               'O3 AQI': 'mean'}) \
        .plot(lw=2, colormap='jet', marker='.', markersize=10, ax=ax)
    ax.set_title('Mean Pollutant AQI Over Time')
    ax.set(xlabel="Average AQI", ylabel="Year")
    plt.savefig(out_path + "/global_mean_pollutant.png")

def explore_top_n(pds, n=4):
    out_path = path.join(path.dirname(path.abspath(__file__)), '../outputs')
    create_output_dir(out_path)
    pds_grouped = pds.groupby(level='State')
    fig = plt.figure(figsize=(12, 8))
    fig.suptitle('AQI in top ' + str(n) +' States', fontsize=14)
    subplots = [221, 222, 223, 224]
    pollutants = ['NO2', 'O3', 'SO2', 'CO']

    for s, p in zip(subplots, pollutants):
        plt.subplot(s)
        pg = pds_grouped[p + ' AQI']
        top = pg.mean().nlargest(n).index
        for i in range(len(top)):
            pg.get_group(top[i]).groupby(pd.Grouper(level='Date Local', freq='M')).mean().plot()
        plt.legend(top, loc=3, fontsize='small')
        plt.title(p + ' AQI')

    plt.tight_layout()
    plt.savefig(out_path + "/top_AQI.png")

def generate_pollution_animation(pds):
    out_path = path.join(path.dirname(path.abspath(__file__)), '../outputs')
    create_output_dir(out_path)

    # Get all the states
    all_states = pds.index.levels[0]
    all_dates = pds.index.levels[1]
    date_range = pd.date_range(all_dates.min(), all_dates.max(), freq='MS')

    flt = (pds.groupby('State')
           .apply(lambda x: x.reset_index(level=1)
                  .resample('MS', on='Date Local')
                  .max()
                  .reindex(date_range))
           )

    generate_us_animation(flt, all_states, out_path + '/us_animation.gif')

def trend_analysis_for_state(pds, state, model='additive'):
    out_path = path.join(path.dirname(path.abspath(__file__)), '../outputs')
    create_output_dir(out_path)
    # Get the data for the given state
    cal = pds.loc[state]
    flt = cal.groupby(pd.Grouper(level='Date Local', freq='M')).agg({'SO2 AQI': 'mean',
                                                                     'CO AQI': 'mean',
                                                                     'NO2 AQI': 'mean',
                                                                     'O3 AQI': 'mean'})

    pollutants = ['SO2', 'CO', 'O3', 'NO2']
    for p in pollutants:
        decompose = sm.tsa.seasonal_decompose(flt[p + ' AQI'], model=model)
        _ = decompose.plot()
        plt.savefig(out_path + '/' + p + '_trend.png')

def predict_for_state_with_prophet(pds, state):
    out_path = path.join(path.dirname(path.abspath(__file__)), '../outputs')
    create_output_dir(out_path)
    # Get the data for the given state
    cal = pds.loc[state]
    flt = cal.groupby(pd.Grouper(level='Date Local', freq='D')).agg({'SO2 AQI': 'mean',
                                                                     'CO AQI': 'mean',
                                                                     'NO2 AQI': 'mean',
                                                                     'O3 AQI': 'mean'})

    pollutants = ['SO2', 'CO', 'O3', 'NO2']
    for p in pollutants:
        copied = flt[p + ' AQI'].copy().to_frame().reset_index()
        # We need to rename the columns to what Prophet expects
        copied = copied.rename(columns={"Date Local": "ds", p + " AQI": "y"})
        model = Prophet()
        model.fit(copied)
        # We want to predict for the next 90 days.
        future = model.make_future_dataframe(periods=90, freq='D')
        forecast = model.predict(future)
        _ = model.plot(forecast)
        plt.savefig(out_path + '/' + p + '_prophet_predict.png')



if __name__ == "__main__":
    poll_ds = load_pollution_dataset()
    plot_global_mean(poll_ds)
    explore_top_n(poll_ds, n=4)
    generate_pollution_animation(poll_ds)
    trend_analysis_for_state(poll_ds, 'California')
    predict_for_state_with_prophet(poll_ds, 'California')

    out_path = path.join(path.dirname(path.abspath(__file__)), '../outputs')
    print("All figures are saved in: ", out_path)



