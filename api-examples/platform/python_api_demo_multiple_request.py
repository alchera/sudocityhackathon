import json
import os

import requests
import websocket
from functools import partial
import logging

logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(name)s\t%(message)s')
logger = logging.getLogger()
logger.setLevel(logging.getLevelName(os.environ.get('LOGGING_LEVEL', 'INFO')))


ALCHERA_WEBSOCKET_URL = 'wss://stream.dev.alchera.tech/query/subscriptions/'
ALCHERA_QUERY_API_URL = ' https://stream.dev.alchera.tech/query/'

# TODO Set these yourself
USERNAME = 'api_demo_user'
PASSWORD = 'Set_This'

# TODO After the first run, switch this to false and paste the API Key. You don't need to keep creating new users
NEED_TO_REGISTER = True
DEFAULT_API_KEY = 'Set_This_After_First_Run_Or_If_You_Already_Have_One'


def run_example():
    """
    This example demonstrates a number of different request types of the Alchera Data Platform API

    1. Registering as a user to get credentials for authentication: an API key
    2. Getting the details of all sensor providers
    3. Filter for sensors / cameras based on the provider and location
    4. Get the measurements / data for these cameras within a specified time frame
    5. Subscribe to live updates from any of the sensors within the filter
    """

    api_key = DEFAULT_API_KEY

    '''
    1.  Submitting a request to register as a user of the API
    '''
    logger.info("###################  PART 1  ###################")

    if NEED_TO_REGISTER:
        register_request_result = requests.post(url=ALCHERA_QUERY_API_URL + 'users/',
                                                json={
                                                    'username': USERNAME,
                                                    'password': PASSWORD
                                                })
        if register_request_result.status_code != 201:
            logger.error("Failed to register as user:\n{}".format(register_request_result.text))
            return
        else:
            logger.info(
                "Successfully registered as API user. Registration Response:\n{}".format(register_request_result.text))

            # the default API key generated during registration can be used to authenticate from now on
            api_key = register_request_result.json().get('default_api_key')

    '''
    2.  Submitting a request to get all of the different providers of sensors
        (cameras which are analysed count as sensors)
        The api_key from registration is set in the X-API-KEY header for authentication purposes
    '''
    logger.info("###################  PART 2  ###################")

    get_provider_request_result = requests.get(url=ALCHERA_QUERY_API_URL + 'providers/',
                                               headers={'X-API-KEY': api_key})

    if get_provider_request_result.status_code != 200:
        logger.error("Failed to get providers:\n{}".format(get_provider_request_result.text))
        return
    else:
        logger.info("Successfully got sensor providers:\n{}".format(get_provider_request_result.text))

    '''
    3.  Submitting a request to get the sensor details of all sensors that fall within a user specified filter 
        The filter will only look for sensors of a specific provider (Tfl; id=3)
        and also only look within a certain area, as defined by the longitude and latitude of vertices of a polygon
        The area in the example is covers Greater London, but larger or smaller polygons are also completely valid.
    '''
    logger.info("###################  PART 3  ###################")

    filter_sensors_request_result = requests.post(url=ALCHERA_QUERY_API_URL + 'sensors/',
                                                  headers={'X-API-KEY': api_key},
                                                  json={
                                                      "provider_id": 1,
                                                      "fence": {
                                                          "type": "polygon",
                                                          "vertices": [
                                                              {
                                                                  "latitude": 51.754149894560754,
                                                                  "longitude": -0.6452625949387993
                                                              },
                                                              {
                                                                  "latitude": 51.760100328809024,
                                                                  "longitude": 0.46023667263932566
                                                              },
                                                              {
                                                                  "latitude": 51.20421090670719,
                                                                  "longitude": 0.36410630154557566
                                                              },
                                                              {
                                                                  "latitude": 51.199908552165375,
                                                                  "longitude": -0.6191700656419243
                                                              }
                                                          ]
                                                      }
                                                  }
                                                  )

    if filter_sensors_request_result.status_code != 200:
        logger.error("Failed to filter sensors:\n{}".format(filter_sensors_request_result.text))
        return
    else:

        number_of_sensors_found = len(filter_sensors_request_result.json().get('sensors'))
        logger.info("Successfully filtered sensors. Number of sensors found: {}".format(number_of_sensors_found))

        if number_of_sensors_found > 0:
            logger.info(
                "Example sensor / camera (logging them all would clutter the output of this example):\n{}".format(
                    filter_sensors_request_result.json().get('sensors')[0]))

    # Creating a list of just the sensor_id from each of the sensors the filter found.
    # This will be useful for the following requests
    sensor_ids = [sensor.get('id') for sensor in filter_sensors_request_result.json().get('sensors')]

    '''
    4.  Submit a request to retrieve the measurements / data from these cameras within a specified time frame
        This is a POST request in which the list of sensor_ids is specified in the body
    '''
    logger.info("###################  PART 4  ###################")

    measurements_request_response = requests.post(url=ALCHERA_QUERY_API_URL + 'sensors/data/',
                                                  headers={'X-API-KEY': api_key},
                                                  json={
                                                      'sensors': sensor_ids,
                                                      'start_time': '2019-05-06T08:00:00Z',
                                                      'end_time': '2019-05-06T10:00:00Z',
                                                      'limit': 10
                                                  }
                                                  )

    if measurements_request_response.status_code != 200:
        logger.error("Failed to get sensor measurements data:\n{}".format(measurements_request_response.text))
        return
    else:

        number_of_measurements = len(measurements_request_response.json().get('measurements'))
        logger.info(
            "Successfully fetched measurement data. Number of measurements found: {}".format(number_of_measurements))

        if number_of_measurements > 0:
            logger.info(
                "Example measurement (logging them all would clutter the output of this example):\n{}".format(
                    measurements_request_response.json().get('measurements')[0]))

    '''
    5.  Opening a web socket and submitting a request to subscribe to updates for the sensor_ids found above
        The on_message method further below can be edited to access the update
        By default the web socket will stay open until the user stops the example program
    '''
    logger.info("###################  PART 5  ###################")

    # Creating the subscribe message json
    subscribe_message_dict = {
        "subscribe": {
            "sensors": sensor_ids
        }
    }

    # convert into readable json string
    subscribe_message_json = json.dumps(subscribe_message_dict)

    logger.info("Initialising web socket connection:")
    start_websocket(websocket_url=ALCHERA_WEBSOCKET_URL,
                    api_key=api_key,
                    subscribe_message=subscribe_message_json)


def start_websocket(websocket_url, api_key, subscribe_message):
    websocket.enableTrace(True)

    on_open_partial = partial(on_open, subscribe_message=subscribe_message)

    ws = websocket.WebSocketApp(url=websocket_url,
                                header={'x-api-key': api_key},
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close)
    ws.on_open = on_open_partial
    ws.run_forever(ping_interval=30, ping_timeout=10)


def on_message(ws, message):
    logger.info('Message received via WebSocket: ' + str(message))
    # Add code here to do something with each message as it is received


def on_error(ws, error):
    logger.error('WebSocket Error:' + str(error))


def on_close(ws):
    logger.info("### WebSocket Closed ###")


def on_open(ws, subscribe_message):
    logger.info("### WebSocket Opened ###")
    ws.send(subscribe_message)


if __name__ == "__main__":

    run_example()

    logger.info('Example process exiting.')
