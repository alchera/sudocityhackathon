# README #

### Description ###
Time series analysis demo using Python.

### Installation ###
The recommended method of installaing the software is either through a 
virtual environment like Anaconda.

##### Anaconda

The recommended way to install on a standalone machine is through the Anaconda python distribution. 
Anaconda can be downloaded from [here](https://www.anaconda.com/download/). 

Once Anaconda is installed, create a python 3.7 environment with pre-installed numpy as follows:

```python
conda create --name sudocity python=3.7
```
Once the anaconda environment is created, activate it with:
```python
source activate sudocity
```
cd into the source directory and install the required conda packages from the command line (Linux, OSX)
```python
bash conda.txt
```
If using Windows, you can execute the following on the command line:
```python
conda install -y basemap
conda install -y -c conda-forge fbprophet
conda install -y -c anaconda statsmodels 
conda install -y -c conda-forge matplotlib 
conda install -y scikit-learn
conda install -c anaconda requests 
```


Additionally, if you want to generate the gif animations, you will need to install imagemagick as well from [here](https://imagemagick.org/index.php)

#### Running
To replicate the results, run the following from one level above the project directory.
```python
python -m alts.apps.pollution
```

If you receive an err or like `KeyError: 'PROJ_LIB'`, please reinstall `basemap` with `conda install basemap`.

The Gaussian Process demo can be run with 
```python
python -m alts.apps.tfl <api_key>
```
`api_key` iis the Alchera API key. You do not need the API key if you are just  going to use the cached data from the repository.

This should create the plots in the `outputs` folder in the project directory.
